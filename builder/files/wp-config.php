<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'social_mysql');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', 'socialdb:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'edkcAZrx/PJphsRvfrSaFGoY8BpYcQrEFBiBCbk6qFe1T7oyKvOj8A==');
define('SECURE_AUTH_KEY',  '9tX1XmuNufxFCtaxmmYEW+XfyfSdaEOhxw4v3CH4mao4Y9HXXnrUew==');
define('LOGGED_IN_KEY',    'daEBDEzqLa0WTf4oHTPW3/s71tujKhws9EISOkL3XChZ3XumGS5FbA==');
define('NONCE_KEY',        'bp0kkiwkLY0fhI7mylBtERXhOD5jPO98dNdYtue5ksYrBmIEF0F64A==');
define('AUTH_SALT',        '0Z9TMZSUzie9bBJIYjED+fVXfejycD8mD/ybpp/6KhA4Nzo7gISx/w==');
define('SECURE_AUTH_SALT', 'jxpfiK/139vyoeWVUhqdofplRoiELo54TM0jrD8k4zUTkuurIIV+mQ==');
define('LOGGED_IN_SALT',   'x982cNZer0X85IDaS+0sGwRz4jqwp/rhcqSctiwJP96qu/kQvgY40Q==');
define('NONCE_SALT',       'Ee0y29fRB917x97+gn7sDWa+vs4unpohLTl2H2Kn8jyWFO8ZnRSWyQ==');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

